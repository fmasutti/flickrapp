//
//  DataBase.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 19/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

import Foundation
import CoreData

class DataBase {
    
    static let shared = DataBase()
    
    private init() {
        
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FlickrApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("\(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Core Data query support using Generics
    @nonobjc public class func queryByProperty<T:NSManagedObject> (entity: T.Type, propertKey:String, propertValue:Any) -> T? where T: NSManagedObject {
        let fetchRequest = NSFetchRequest<T>(entityName: NSStringFromClass(entity));
        fetchRequest.predicate = NSPredicate(format: "\(propertKey) == [c] %@", "\(propertValue)")
        do {
            let itens = try DataBase.shared.persistentContainer.viewContext.fetch(fetchRequest)
            return itens.first
        } catch {
            let saveError = error as NSError
            print("\(saveError), \(saveError.userInfo)")
            return nil
        }
    }
}
