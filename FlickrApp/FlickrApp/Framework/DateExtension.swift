//
//  DateExtensions.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 20/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

import Foundation

extension NSDate {
    var stringValue: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self as Date)
    }
}
