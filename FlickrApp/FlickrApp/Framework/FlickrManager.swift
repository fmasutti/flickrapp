//
//  FlickrManager.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 19/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

//Json objects
struct PhotoJson {
    
    private var farm: String
    private var secret: String
    private var server: String
    
    var id: String
    var title: String
    var photoDescription: String
    var image: UIImage?
    var ownerName: String
    var postUpload: NSDate
    
    init(jsonObj: JSON) {
        self.farm = jsonObj["farm"].stringValue
        self.secret = jsonObj["secret"].stringValue
        self.server = jsonObj["server"].stringValue
        self.id = jsonObj["id"].stringValue
        self.title = jsonObj["title"].stringValue
        self.photoDescription = jsonObj["description"]["_content"].stringValue
        self.ownerName  = jsonObj["ownername"].stringValue
        self.postUpload =  NSDate(timeIntervalSince1970: jsonObj["dateupload"].doubleValue)
    }
    
    private var imageBaseURL: String {
        get {
            let baseURLString = "http://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)"
            return baseURLString
        }
    }
    
    //More info about images sizes/url in https://www.flickr.com/services/api/misc.urls.html
    var imageURLSmall: URL {
        get {
            return URL(string: "\(self.imageBaseURL)_n.jpg")!
        }
    }
    
    var imageURLMedium: URL {
        get {
            return URL(string: "\(self.imageBaseURL)_c.jpg")!
        }
    }
    
    var imageURLLarge: URL {
        get {
            return URL(string: "\(self.imageBaseURL)_h.jpg")!
        }
    }
    
    var imageThumbnail: URL {
        get {
            return URL(string: "\(self.imageBaseURL)_t.jpg")!
        }
    }
}

import Foundation
import Alamofire
import SwiftyJSON

class FlickerManager {
    
    private let HEADER: HTTPHeaders = [
        "Accept": "application/json"
    ]
    private let BASE_SERVER_URL:String = "https://api.flickr.com/services/rest/"
    
    private let BASE_PAREMETERS: [String: Any] = ["api_key": "6c8e5516d773dc6dd3575f0fb3164ef8", "format":"json", "nojsoncallback": 1]
    
    static let shared = FlickerManager()
    private init() {
        
    }
    
    //https://www.flickr.com/services/api/flickr.photos.getRecent.html //More infos about this method.
    func getRecentPhotos(per_page: Int?, page: Int?, photoImageDownloadBlock:@escaping (_ photo: PhotoJson) -> Void, completionMetadataBlock:@escaping (_ photos:[PhotoJson]?,_ error:Error?) -> Void) {
        
        var parameters = BASE_PAREMETERS
        parameters.updateValue("flickr.photos.getRecent", forKey: "method") //methodParam
        parameters.updateValue("owner_name, date_upload, description", forKey: "extras") //Aditional needed
        
        if let validPerPage = per_page {
            parameters.updateValue(validPerPage, forKey: "per_page")
        }
        
        if let validPage = page {
            parameters.updateValue(validPage, forKey: "page")
        }
        
        Alamofire.request(BASE_SERVER_URL, parameters: parameters, headers: HEADER).responseJSON { response in
            switch response.result {
            case .success:
//                print("Validation Successful")
                if let data = response.data {
                    let jsonData:JSON = JSON(data: data)
//                print("jsonData: \(jsonObj)")
                    let photosDict = jsonData["photos"]
                    let photoArray = photosDict["photo"]
                    var photos = [PhotoJson]()
                    for item in photoArray  {
                        var photo = PhotoJson(jsonObj: item.1)
                        self.downloadImageFrom(url: photo.imageThumbnail, completionBlock: { (image) -> Void in
                            photo.image = image
                            photoImageDownloadBlock(photo)
                        })
                        photos.append(photo)
                    }
                    completionMetadataBlock(photos, nil)
                }
            case .failure(let error):
                print(error)
                completionMetadataBlock(nil, error)
            }
        }
    }
    
    func downloadImageFrom(url: URL, completionBlock:@escaping (_ image: UIImage?) -> Void?) {
        Alamofire.request(url).responseData { response in
            switch response.result {
            case .success(let data):
                if let image = UIImage(data: data) {
                    completionBlock(image)
                }else {
                    completionBlock(nil)
                }
            case .failure(let error):
                print(error)
                completionBlock(nil)
            }
        }
    }
}

