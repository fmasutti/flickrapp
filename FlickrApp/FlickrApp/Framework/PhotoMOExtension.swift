//
//  PictureMOExtension.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 19/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Extension for my own coredata table.
extension Photo {
    
    static func getAllPicturesOrderByPostDate() -> [Photo]? {
        let fechrequest:NSFetchRequest<Photo> = Photo.fetchRequest()
        fechrequest.sortDescriptors = [NSSortDescriptor(key: "postDate", ascending: false)]
        do {
            let picturesList = try DataBase.shared.persistentContainer.viewContext.fetch(fechrequest)
            return picturesList
        } catch {
            let saveError = error as NSError
            print("\(saveError), \(saveError.userInfo)")
            return nil
        }
    }
    
    static func createOrUpdate(photoJson:PhotoJson, hasToSaveContext:Bool) -> Photo {
        var photoMO: Photo
        if let photoFromDB:Photo  = DataBase.queryByProperty(entity: Photo.self, propertKey: "photoId", propertValue: photoJson.id) {
            photoMO = photoFromDB
        }else {
            print("Photo from \(photoJson.ownerName) added on DB")
            photoMO = Photo(context: DataBase.shared.persistentContainer.viewContext)
        }
        if let image = photoJson.image {
            if let imageData = UIImagePNGRepresentation(image) {
                photoMO.image = imageData as NSData
            }
        }
        
        photoMO.ownerName = photoJson.ownerName
        photoMO.photoDescription = photoJson.photoDescription
        photoMO.postDate = photoJson.postUpload
        photoMO.title = photoJson.title
        photoMO.imageURL = photoJson.imageURLMedium.absoluteString
        
        if hasToSaveContext {
            DataBase.shared.saveContext()
        }
        return photoMO
    }
    
    static func createOrUpdateListContent(photoList:[PhotoJson]) {
        for photo in photoList {
            let _ = Photo.createOrUpdate(photoJson: photo, hasToSaveContext: false)
        }
        DataBase.shared.saveContext()
    }
}
