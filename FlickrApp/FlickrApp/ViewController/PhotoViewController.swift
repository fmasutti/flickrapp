//
//  PhotoViewController.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 20/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

import Foundation
import UIKit

class PhotoViewController: UIViewController {
    @IBOutlet weak var lblOwerName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPostDate: UILabel!
    var photo: Photo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let photo = self.photo {
            self.updateContent(newPhoto: photo)
        }else {
            self.lblTitle.text = ""
            self.lblOwerName.text = ""
            self.lblPostDate.text = ""
            self.lblDescription.text = ""
//            self.imgPhoto.image = nil
        }
    }
    
    func updateContent(newPhoto:Photo) {
        self.photo = newPhoto;
        self.lblTitle.text = self.photo!.title
        self.lblOwerName.text = self.photo!.ownerName
        self.lblPostDate.text = self.photo!.postDate?.stringValue
        self.lblDescription.text = self.photo!.photoDescription
        if let validData = self.photo!.image {
            self.imgPhoto.image = UIImage(data: validData as Data)
        }
    }
    
}
