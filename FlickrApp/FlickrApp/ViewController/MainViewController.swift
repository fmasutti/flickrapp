//
//  ViewController.swift
//  FlickrApp
//
//  Created by Frantiesco Masutti on 19/10/16.
//  Copyright © 2016 Cold Mass. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    var imageViewer1: PhotoViewController!
    var imageViewer2: PhotoViewController!
    var photoList:[Photo]?
    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadContentAndSaveOnDB()
        
        if let photosFromDB = Photo.getAllPicturesOrderByPostDate() {
            self.photoList = photosFromDB
        }
    }
    
    func updateViewers() {
        DispatchQueue.main.async {
            self.imageViewer1.updateContent(newPhoto: self.photoList![self.currentIndex])
            if self.currentIndex + 1 <= (self.photoList?.count)! {
                self.imageViewer2.updateContent(newPhoto: self.photoList![self.currentIndex + 1])
            }
        }
    }
    
    func loadContentAndSaveOnDB() {
        FlickerManager.shared.getRecentPhotos(per_page: 2, page: 2, photoImageDownloadBlock: { (photo) in
            _ = Photo.createOrUpdate(photoJson: photo, hasToSaveContext: true)
            self.updateViewers();
        }) { (photoList, error) in
            if let allPhotos = photoList {
                //save photos on db and update content List
                Photo.createOrUpdateListContent(photoList: allPhotos)
                if let photosFromDB = Photo.getAllPicturesOrderByPostDate() {
                    self.photoList = photosFromDB
                    self.updateViewers();
                }
            }
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func updateContent(photoViewer:PhotoViewController) {
        photoViewer.updateContent(newPhoto: self.photoList![currentIndex])
    }
    
    @IBAction func swipeItemLeft(_ sender: AnyObject) {
        
        if self.currentIndex > 0 {
            self.currentIndex = self.currentIndex - 1
        }
    }
    
    @IBAction func swipeItemRight(_ sender: AnyObject) {
        if self.currentIndex < self.photoList!.count {
            self.currentIndex = self.currentIndex + 1
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SeguePhoto1" {
            let dvc = segue.destination as! PhotoViewController
            self.imageViewer1 = dvc
        }else if segue.identifier == "SeguePhoto2" {
            let dvc = segue.destination as! PhotoViewController
            self.imageViewer2 = dvc
        }
    }

}

